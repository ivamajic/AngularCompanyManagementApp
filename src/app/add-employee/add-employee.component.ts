import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee';
import {EmployeeService} from '../employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  employees: Employee[];
  constructor(
    private employeeService: EmployeeService,
    private router: Router
  ) { }

  ngOnInit() {
    document.getElementById("nav").style.display="block";
    this.getEmployees;

  }

  getEmployees(): void {
    this.employeeService.getEmployees().subscribe(employees=>this.employees=employees);
  }

  addEmployee(firstname: string, lastname: string, picture: string, position: string): void {
    firstname = firstname.trim();
    lastname = lastname.trim();
    picture = picture.trim();
    position = position.trim();
    if (!firstname || !lastname || !picture || !position)  {
      alert("You have an empty field!");
      return;}
    this.employeeService.addEmployee({firstname, lastname, picture, position } as Employee).subscribe(employee => {this.employees.push(employee);});
    this.router.navigate(['employees']);
    
    

  }

}
