import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {EmployeesComponent} from './employees/employees.component'
import {DetailsComponent} from './details/details.component'
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { HomeComponent } from './home/home.component';
import { InfoComponent } from './info/info.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'employees', component: EmployeesComponent},
  {path: 'add-new', component: AddEmployeeComponent},
  {path: 'details/:id', component: DetailsComponent},
  {path: 'search', component: SearchComponent},
  {path: 'info', component: InfoComponent},
  {path: 'home', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRouterModule { }
