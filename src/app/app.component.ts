import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'Company Management';
    constructor() {
  }

  hideNav():void {
    document.getElementById("nav").style.display="none";
  }
}
