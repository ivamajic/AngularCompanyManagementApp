import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './in-memory-data.service';


import { AppComponent } from './app.component';
import { EmployeesComponent } from './employees/employees.component';
import { DetailsComponent } from './details/details.component';
import { EmployeeService } from './employee.service';
import { AppRouterModule } from './/app-router.module';
import { HttpClientModule } from '@angular/common/http';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { HomeComponent } from './home/home.component';
import { InfoComponent } from './info/info.component';
import { SearchComponent } from './search/search.component';


@NgModule({
  declarations: [
    AppComponent,
    EmployeesComponent,
    DetailsComponent,
    EmployeeEditComponent,
    AddEmployeeComponent,
    HomeComponent,
    InfoComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRouterModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService,{dataEncapsulation:false})
  ],
  providers: [
    EmployeeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
