import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import {Employee} from '../employee';
import {EmployeeService} from '../employee.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  employee: Employee;
  showVar: boolean = false;
  bText: string='Edit';

  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService,
    private location: Location
  ) { }

  ngOnInit() {
    document.getElementById("nav").style.display="block";
    this.getEmployee();
  }

  goBack(): void { 
    this.location.back();
  } 

  getEmployee(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employeeService.getEmployee(id).subscribe(employee=>this.employee=employee);
  }

  toggleEdit(employee: Employee): void {
    this.showVar = !this.showVar;
    this.employee = employee;
    if (this.showVar==true) {
      this.bText='Cancel';
    }
    else this.bText='Edit';
  }

}
