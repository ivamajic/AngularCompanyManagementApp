import { Component, OnInit, Input } from '@angular/core';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent implements OnInit {
  @Input() showMe: boolean;
  @Input() employee: Employee;
  employees: Employee[];
  
  constructor(private employeeService: EmployeeService,
    private location: Location
  ) {}

  ngOnInit() {
    document.getElementById("nav").style.display="block";
    this.getEmployees();
  }

  getEmployees(): void {
    this.employeeService.getEmployees().subscribe(employees=>this.employees=employees);
  }

  goBack(): void { 
    this.location.back();
  } 

  saveChanges(): void {
    this.employeeService.updateEmployee(this.employee).subscribe(()=>this.goBack());
  }

  deleteEmployee(employee): void {
    console.log("Message");
    this.employees = this.employees.filter(e=>e !== employee);
    this.employeeService.deleteEmployee(employee).subscribe(()=>this.goBack());
  }

}
