import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import {Employee} from './employee';

const httpOptions={headers: new HttpHeaders({'Content-Type': 'application/json'})};

@Injectable()
export class EmployeeService {
  private url='api/employees';

  constructor(
    private http: HttpClient
  ) { }

  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.url);
  }

  getEmployee(id: number): Observable<Employee> {
    const url = `${this.url}/${id}`;
    return this.http.get<Employee>(url)
  }

  updateEmployee(employee: Employee): Observable<any> {
    return this.http.put(this.url, employee, httpOptions);
  }

  addEmployee (employee: Employee): Observable<Employee> {
    return this.http.post<Employee>(this.url, employee, httpOptions);
  }

  deleteEmployee(employee:Employee|number): Observable<Employee> {
    const id = typeof employee === 'number' ? employee : employee.id;
    const url = `${this.url}/${id}`;
    return this.http.delete<Employee>(url, httpOptions);
  }

  searchByName(name: string): Observable<Employee[]> {
    if (!name.trim()) {
      return of([]);
    }
    return this.http.get<Employee[]>(`api/employees/?lastname=${name}`);
  }

  searchByPosition(position: string): Observable<Employee[]> {
    if (!position.trim()) {
      return of([]);
    }
    return this.http.get<Employee[]>(`api/employees/?position=${position}`);
  }

  searchById(id: string): Observable<Employee[]> {
    if (!id.trim()) {
      return of([]);
    }
    return this.http.get<Employee[]>(`api/employees/?id=${id}`);
  }

}
