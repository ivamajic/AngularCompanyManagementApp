export class Employee {
    id: number;
    firstname: string;
    lastname: string;
    picture: string;
    position: string;
}