import { Component, OnInit } from '@angular/core';
import {Employee} from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  employees: Employee[];
  rows: number[]=[];
  nav: HTMLElement;

  constructor(
    private employeeService: EmployeeService
  ) { }

  getEmployees(): void {
    this.employeeService.getEmployees().subscribe(employees=>this.employees=employees);

  }

  ngOnInit() {
    document.getElementById("nav").style.display="block";
    this.getEmployees();
    this.rows=Array.from(Array(Math.ceil(100/5)).keys());
  }
}
