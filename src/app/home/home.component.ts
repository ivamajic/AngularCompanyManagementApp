import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  nav:HTMLElement;
  constructor() { }

  ngOnInit() {
    this.nav=document.getElementById("nav");
  }
  showNav():void {
    this.nav.style.display="block";
  }

}