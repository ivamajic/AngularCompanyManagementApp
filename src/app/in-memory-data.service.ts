import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const employees = [
      { id: 1, firstname: 'Ema', lastname: 'Emić', picture: 'assets/images/pic1.jpg', position: 'CEO'},
  { id: 2, firstname: 'Mario', lastname: 'Maričić', picture: 'assets/images/pic2.jpg', position: 'Project Manager'},
  { id: 3, firstname: 'Ivan', lastname: 'Ivić', picture: 'assets/images/pic3.jpg', position: 'Designer'},
  { id: 4, firstname: 'Josip', lastname: 'Josipović', picture: 'assets/images/pic4.jpg', position: 'Designer'},
  { id: 5, firstname: 'Zlatko', lastname: 'Zlatić', picture: 'assets/images/pic5.jpg', position: 'Frontend Developer'},
  { id: 6, firstname: 'Petra', lastname: 'Petrić', picture: 'assets/images/pic6.jpg', position: 'Frontend Developer'},
  { id: 7, firstname: 'Andrija', lastname: 'Andrić', picture: 'assets/images/pic7.jpg', position: 'Backend Developer'},
  { id: 8, firstname: 'Lucija', lastname: 'Lukić', picture: 'assets/images/pic8.jpg', position: 'Backend Developer'},
  { id: 9, firstname: 'Franjo', lastname: 'Franjić', picture: 'assets/images/pic9.jpg', position: 'Backend Developer'},
  { id: 10, firstname: 'Marta', lastname: 'Martić', picture: 'assets/images/pic10.jpg', position: 'Backend Developer'}
    ];
    return {employees};
  }
}