import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { of } from 'rxjs/observable/of';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';

import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  nSearch: HTMLElement;
  pSearch: HTMLElement;
  iSearch: HTMLElement;
  nSearchField: HTMLElement;
  pSearchField: HTMLElement;
  iSearchField: HTMLElement;
  employees$: Observable<Employee[]>;
  private terms = new Subject<string>();

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {

    document.getElementById("nav").style.display="block";

    this.nSearch=document.getElementById("nSearch");
    this.pSearch=document.getElementById("pSearch");
    this.iSearch=document.getElementById("idSearch");

    this.nSearchField=document.getElementById("nSearchField");
    this.pSearchField=document.getElementById("pSearchField");
    this.iSearchField=document.getElementById("idSearchField");
  }

  nameSearchInit(): void {
    this.nSearch.style.display="none";
    this.pSearch.style.display="inline-block";
    this.iSearch.style.display="inline-block";

    this.nSearchField.style.display="inline-block";
    this.pSearchField.style.display="none";
    this.iSearchField.style.display="none";

    this.employees$=this.terms.pipe(debounceTime(50),distinctUntilChanged(),switchMap((term: string)=>this.employeeService.searchByName(term)),);
   
  }

  nameSearch(name:string): void {
    this.terms.next(name);
  }

  positionSearchInit(): void {
    this.nSearch.style.display="inline-block";
    this.pSearch.style.display="none";
    this.iSearch.style.display="inline-block";

    this.nSearchField.style.display="none";
    this.pSearchField.style.display="inline-block";
    this.iSearchField.style.display="none";

    this.employees$=this.terms.pipe(debounceTime(50),distinctUntilChanged(),switchMap((term: string)=>this.employeeService.searchByPosition(term)),);
  }

  positionSearch(position:string): void {
    this.terms.next(position);
  }

  idSearchInit(): void {
    this.nSearch.style.display="inline-block";
    this.pSearch.style.display="inline-block";
    this.iSearch.style.display="none";

    this.nSearchField.style.display="none";
    this.pSearchField.style.display="none";
    this.iSearchField.style.display="inline-block";

    this.employees$=this.terms.pipe(debounceTime(50),distinctUntilChanged(),switchMap((term: string)=>this.employeeService.searchById(term)),);
  }

  idSearch(id:string): void {
    this.terms.next(id);
  }

}
